package Exceptions;

public class AlreadyHaveItemsException extends Exception {
    public AlreadyHaveItemsException(String message){
        super(message);
    }
}
