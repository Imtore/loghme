package Exceptions;

public class NotEnoughCredit extends Exception{
    public NotEnoughCredit(String message){
        super(message);
    }
}
