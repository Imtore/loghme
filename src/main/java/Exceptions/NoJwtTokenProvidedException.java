package Exceptions;

public class NoJwtTokenProvidedException extends Exception {
    public NoJwtTokenProvidedException(String message){
        super(message);
    }
}
