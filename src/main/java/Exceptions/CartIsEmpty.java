package Exceptions;

public class CartIsEmpty extends Exception{
    public CartIsEmpty(String message){
        super(message);
    }
}
