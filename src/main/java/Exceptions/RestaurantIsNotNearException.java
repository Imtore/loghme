package Exceptions;

public class RestaurantIsNotNearException extends Exception{
    public RestaurantIsNotNearException(String message){
        super(message);
    }
}
