package Utilities;
import Domain.FoodParty.GetFoodPartyJob;
import Domain.Restaurant.InitializeRestaurantsJob;
import Domain.Restaurant.RestaurantManager;
import Domain.Delivery.AssignDelivery;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
    @WebListener
    public class BackgroundJobManager implements ServletContextListener {

        private ScheduledExecutorService scheduler;

        @Override
        public void contextInitialized(ServletContextEvent event) {
            InitializeRestaurantsJob.doJob();
            scheduler = Executors.newSingleThreadScheduledExecutor();
            scheduler.scheduleAtFixedRate(new AssignDelivery(), 0, 30, TimeUnit.SECONDS);
            scheduler.scheduleAtFixedRate(new GetFoodPartyJob(), 0, 30, TimeUnit.MINUTES);
        }

        @Override
        public void contextDestroyed(ServletContextEvent event) {
            scheduler.shutdownNow();
        }

    }

