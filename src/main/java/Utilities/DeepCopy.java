package Utilities;

import java.util.HashMap;
import java.util.Map;

public class DeepCopy {
    public static HashMap<String, Integer> copyMap(HashMap<String, Integer> original) {
        HashMap<String, Integer> copy = new HashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : original.entrySet())
        {
            copy.put(entry.getKey(), entry.getValue());
        }
        return copy;
    }

}
