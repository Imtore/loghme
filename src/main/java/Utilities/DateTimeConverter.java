package Utilities;

import java.time.LocalDateTime;

public class DateTimeConverter {
    public static String localDateTimeToSQL(String input){
        String inputS = input.toString();
        return inputS.substring(0,10)+ " " +inputS.substring(11,19);
    }

    public static String SQLToLocalDateTime(String input){
        return input.substring(0,10)+ "T" +input.substring(11,19)+".000000";
    }
}
