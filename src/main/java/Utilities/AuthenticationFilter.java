package Utilities;



import Exceptions.NoJwtTokenProvidedException;
import io.jsonwebtoken.JwtException;

import javax.servlet.*;
import java.io.IOException;
import java.util.ArrayList;

import java.util.List;
import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static Security.JwtTokenProvider.*;
import static java.lang.Thread.sleep;

public class AuthenticationFilter implements Filter {

    private ArrayList<String> excludedUrls = new ArrayList<String>();

    public AuthenticationFilter() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @see Filter#destroy()
     */
    public void destroy() {
        // TODO Auto-generated method stub
    }

    /**
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        String path = ((HttpServletRequest) request).getServletPath();
        if(!excludedUrls.contains(path))
        {
            try {
                String token = resolveToken((HttpServletRequest) request);
                if (validateToken(token))
                {
                    String userId = getUserID(token);
                    request.setAttribute("userId",userId);
                }
            } catch (NoJwtTokenProvidedException e) {
                ((HttpServletResponse)response).sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
                return;
            } catch (JwtException | IllegalArgumentException e)
            {
                ((HttpServletResponse)response).sendError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
                return;
            }
        }

        chain.doFilter(request, response);

    }

    /**
     * @see Filter#init(FilterConfig)
     */
    public void init(FilterConfig fConfig) throws ServletException {
        excludedUrls.add("/auth/signup");
        excludedUrls.add("/auth/login");
        excludedUrls.add("/auth/google");
        excludedUrls.add("/index.jsp");
    }
}
