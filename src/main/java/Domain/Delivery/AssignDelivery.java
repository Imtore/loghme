package Domain.Delivery;

import Repository.DeliveryDAO;
import Repository.User.OrderDAO;
import Repository.User.UserRepository;
import Repository.User.OrderStatus;

import java.util.ArrayList;

public class AssignDelivery implements Runnable {
    @Override
    public void run() {
        System.out.println("AssignDelivery JOB: cheking for deliveries");
        ArrayList<DeliveryDAO> deliveries = DeliveryManager.getInstance().getAvailableDeliveries();
        if (deliveries != null & deliveries.size()>0)
        {
            System.out.println("AssignDelivery JOB: found deliveries, cheking for orders!");
            ArrayList<OrderDAO> orders = DeliveryManager.getInstance().getNotAssignedOrders();
            for(OrderDAO order:orders)
            {
                DeliveryDAO best_delivery = DeliveryManager.getInstance().findBestDelivery(deliveries,order);
                order.setDelivery(best_delivery);
                order.setStatus(OrderStatus.Delivering);
                System.out.println("order assigned: " + order.getId() + " delivery time:" +order.getRemainingTime()+ "system time" + order.getlastAssignedTime());

            }

        }
    }
}

