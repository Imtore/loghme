package Domain.Delivery;

import Domain.Restaurant.RestaurantManager;
import Domain.User.UserManager;
import Repository.DeliveryDAO;
import Repository.LocationDAO;
import Repository.User.OrderDAO;
import Repository.User.UserRepository;
import Utilities.GetFromUrl;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


import java.io.IOException;
import java.util.ArrayList;

public class DeliveryManager {
    private static DeliveryManager instance;

    public static DeliveryManager getInstance(){
        if(instance==null)
            instance = new DeliveryManager();
        return instance;
    }

    public ArrayList<DeliveryDAO> getAvailableDeliveries() {
        String json = GetFromUrl.getStringFromURL("http://138.197.181.131:8080/deliveries");
        ObjectMapper mapper = new ObjectMapper();
        try {
            ArrayList<DeliveryDAO> deliveries = mapper.readValue(json, new TypeReference<ArrayList<DeliveryDAO>>(){});
            return deliveries;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<OrderDAO> getNotAssignedOrders()
    {
        return UserRepository.getInstance().getNotAssignedOrders();
    }

    public Double calculateDeliveryTime(LocationDAO restaurantLoc, LocationDAO userLoc, LocationDAO deliveryLoc , Double velocity) {
        Double restDeliveryDistance = Math.sqrt((Math.pow((restaurantLoc.getX() - deliveryLoc.getX()),2)) + (Math.pow((restaurantLoc.getY() - deliveryLoc.getY()),2)));
        Double userRestDistance = Math.sqrt((Math.pow((userLoc.getX() - restaurantLoc.getX()),2)) + (Math.pow((userLoc.getY() - restaurantLoc.getY()),2)));
        Double distance = restDeliveryDistance+userRestDistance;
        return distance/velocity;
    }

    public DeliveryDAO findBestDelivery(ArrayList<DeliveryDAO> deliveries, OrderDAO order) {
        DeliveryDAO bestDelivery = new DeliveryDAO();
        double bestTime = Double.POSITIVE_INFINITY;
        LocationDAO restLoc = RestaurantManager.getInstance().getRestaurantLocation(order.getCart().getRestaurantId());
        LocationDAO userLoc = UserManager.getInstance().getUserLocation(order.getCart().getUserId());
        for (DeliveryDAO delivery : deliveries) {
            double deliveryTime = this.calculateDeliveryTime(restLoc,userLoc,delivery.getLocation(),delivery.getVelocity());
            if( deliveryTime < bestTime)
            {
                bestDelivery = delivery;
                bestTime = deliveryTime;
            }

        }
        UserRepository.getInstance().setOrderStatusToDelivering(order.getId(),bestTime);
        return bestDelivery;
    }
}
