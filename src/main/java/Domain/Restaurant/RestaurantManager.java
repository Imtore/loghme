package Domain.Restaurant;

import Repository.LocationDAO;
import Repository.Restaurant.FoodDAO;
import Repository.Restaurant.RestaurantDAO;
import Repository.Restaurant.RestaurantRepository;
import Repository.User.UserRepository;
import Exceptions.RestaurantNotFoundException;
import java.util.ArrayList;

public class RestaurantManager {
    private static RestaurantManager instance;

    public static RestaurantManager getInstance(){
        if(instance==null)
            instance = new RestaurantManager();
        return instance;
    }

    public void updateRestaurants(ArrayList<RestaurantDAO> restaurants){
        RestaurantRepository repo = RestaurantRepository.getInstance();

        for(RestaurantDAO restaurant: restaurants){
            String restaurantId = restaurant.getId();
            if(!repo.restaurantExists(restaurantId)){
                repo.addRestaurant(restaurant);
                repo.addRestaurantMenu(restaurantId,restaurant.getMenu());
            }else{
                for(FoodDAO foodDAO: restaurant.getMenu()){
                    if(!repo.foodExists(restaurantId,foodDAO.getName() )){
                        int menuSize = repo.getMenuSize(restaurantId);
                        repo.addFood(restaurantId, menuSize, foodDAO);
                    }else{
                        int foodId = repo.getFoodId(restaurantId, foodDAO.getName());
                        repo.updateFood(restaurantId, foodId, foodDAO);
                    }
                }
            }
        }
    }

    public ArrayList<RestaurantDAO> getRestaurants(Integer userId, Integer offset, Integer count){
        LocationDAO location = UserRepository.getInstance().getUserLocation(userId);
        return RestaurantRepository.getInstance().getNearRestaurants(location, offset, count);
    }

    public RestaurantDAO getRestaurantById(String restaurantId) throws RestaurantNotFoundException{
        RestaurantRepository repo = RestaurantRepository.getInstance();
        if(repo.restaurantExists(restaurantId)){
            return repo.getFullRestaurantById(restaurantId);
        }else{
            throw new RestaurantNotFoundException("Restaurant " + restaurantId + " was not found");
        }
    }

    public LocationDAO getRestaurantLocation(String restId)
    {
        return RestaurantRepository.getInstance().getRestaurantLocation(restId);
    }

    public String getRestaurantName(String restaurantId)
    {
        return RestaurantRepository.getInstance().getRestaurantName(restaurantId);
    }

    public Boolean isInFoodParty(Integer foodID, String  restID)
    {
        return RestaurantRepository.getInstance().isInFoodParty(foodID,restID);
    }

    public ArrayList<RestaurantDAO> search(String foodName, String restaurantName){
        if(restaurantName.equals("none")){
            return RestaurantRepository.getInstance().searchForFoodInAll(foodName);
        }
        else if(foodName.equals("none")){
            return RestaurantRepository.getInstance().searchForRestaurant(restaurantName);
        }
        return RestaurantRepository.getInstance().searchForFoodInRestaurant(foodName, restaurantName);
    }
}
