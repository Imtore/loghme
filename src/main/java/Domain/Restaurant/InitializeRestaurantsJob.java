package Domain.Restaurant;

import Repository.Restaurant.RestaurantDAO;
import Utilities.GetFromUrl;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;

public class InitializeRestaurantsJob {
    public static void doJob(){
        ArrayList<RestaurantDAO> restaurants = new ArrayList<>();
        String json = GetFromUrl.getStringFromURL("http://138.197.181.131:8080/restaurants");
        ObjectMapper mapper = new ObjectMapper();
        try {
            restaurants = mapper.readValue(json, new TypeReference<ArrayList<RestaurantDAO>>(){});
            for(RestaurantDAO rest: restaurants){
                rest.setSelfIdInMenuItems();
            }
        } catch (IOException e) {
            System.out.println(e.getStackTrace());
            e.printStackTrace();
        }
        RestaurantManager.getInstance().updateRestaurants(restaurants);
    }
}
