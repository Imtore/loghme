package Domain.User;

import Domain.FoodParty.FoodPartyManager;
import Domain.Restaurant.RestaurantManager;
import Repository.FoodParty.FoodPartyRepository;
import Repository.LocationDAO;
import Repository.Restaurant.RestaurantRepository;
import Repository.User.CartDAO;
import Repository.User.OrderDAO;
import Repository.User.UserDAO;
import Repository.User.UserRepository;
import Repository.User.OrderStatus;
import Exceptions.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.mindrot.jbcrypt.BCrypt;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.Timestamp;
import java.util.*;

import static Security.JwtTokenProvider.createToken;
import static Security.SecurityConstants.CLIENT_ID;


public class UserManager {

    private static Domain.User.UserManager instance;

    public static Domain.User.UserManager getInstance() {
        if (instance == null)
            instance = new Domain.User.UserManager();
        return instance;
    }

    public Integer IncreaseCredit(Integer userID, Integer amount)
    {
        //Todo check if user exists?
        Integer userCredit = UserRepository.getInstance().getUserCredit(userID);
        Integer newCredit = userCredit + amount;
        UserRepository.getInstance().setCredit(userID, newCredit);
        return newCredit;
    }

    public void finalizeOrder(Integer userId) throws CartIsEmpty, NotEnoughCredit
    {
        //Todo check if user exists?
        CartDAO cart = UserRepository.getInstance().getUserNotClosedCart(userId);
        if(cart==null)
        {
            throw new CartIsEmpty("Cart is empty");
        }
        else
        {
            Integer credit = UserRepository.getInstance().getUserCredit(userId);
            Integer total = 0;
            Iterator hmIterator = cart.getCartItems().entrySet().iterator();
            while (hmIterator.hasNext()) {
                Map.Entry mapElement = (Map.Entry)hmIterator.next();
                String foodName = (String)mapElement.getKey();
                Integer foodPrice = cart.getCartItemPrices().get(foodName);
                Integer count = (Integer) mapElement.getValue();
                total+=foodPrice*count;
            }
            if(total > credit)
            {
                throw new NotEnoughCredit("Not enough Credit");
            }
            else
            {
                UserRepository.getInstance().CloseCart(cart.getCartID());
                UserRepository.getInstance().setCredit(userId,credit-total);
                UserRepository.getInstance().CreateOrder(cart.getCartID());
            }
        }
    }


    public CartDAO addToCart(Integer userId, String foodName, String restID, Integer count ) throws AlreadyHaveItemsException, RestaurantIsNotNearException,RestaurantNotFoundException
    {
        CartDAO cart = UserRepository.getInstance().getUserNotClosedCart(userId);
        if(cart == null)
        {
            LocationDAO restLoc = RestaurantRepository.getInstance().getRestaurantLocation(restID);
            if(restLoc == null)
            {
                throw new RestaurantNotFoundException("Restaurant doesn't exist");
            }
            else if(! isNear(UserRepository.getInstance().getUserLocation(userId),restLoc))
            {
                throw new RestaurantIsNotNearException("Restaurant is not close enough");
            }
            else
            {
                cart = UserRepository.getInstance().createCart(userId,restID);
            }

        }
        else
        {
            if(!cart.getRestaurantId().equals(restID))
            {
                throw new AlreadyHaveItemsException("You already have items from another restaurant");
            }
        }
        Integer foodId = RestaurantRepository.getInstance().getFoodId(restID,foodName);
        for(int i =0 ; i< count; i++){

            if(RestaurantManager.getInstance().isInFoodParty(foodId,restID))
            {

                if(!FoodPartyRepository.getInstance().isOver(restID, foodId)){
                    UserRepository.getInstance().addToCart(cart.getCartID(),foodId,restID);
                    FoodPartyManager.getInstance().decrementFoodPartyCount(restID, foodId);
                }
            }
            else{
                UserRepository.getInstance().addToCart(cart.getCartID(),foodId,restID);
            }
        }
        return UserRepository.getInstance().getCart(cart.getCartID());
    }

    public CartDAO deleteFromCart(Integer userId, String foodName, String restID)
    {
        CartDAO cart = UserRepository.getInstance().getUserNotClosedCart(userId);
        Integer foodId = RestaurantRepository.getInstance().getFoodId(restID,foodName);
        UserRepository.getInstance().RemoveFromCart(cart.getCartID(),foodId);
        if(RestaurantManager.getInstance().isInFoodParty(foodId,restID))
        {
            FoodPartyManager.getInstance().incrementFoodPartyCount(restID, foodId);
        }
        CartDAO newCart = UserRepository.getInstance().getCart(cart.getCartID());
        if(UserRepository.getInstance().cartIsEmpty(cart.getCartID()))
        {
            UserRepository.getInstance().removeCart(cart.getCartID());
        }

        return newCart;
    }

    public CartDAO getCart(Integer userId)
    {
        CartDAO cart = UserRepository.getInstance().getUserNotClosedCart(userId);
        if(cart == null)
        {
            CartDAO emptyCart  = new CartDAO();
            emptyCart.setRestaurantId(null);
            emptyCart.setCartID(null);
            emptyCart.setUserId(userId);
            HashMap<String,Integer> cartItems =new HashMap<String, Integer>();
            HashMap <String,Integer> cartItemPrices =new HashMap<String, Integer>();
            emptyCart.setCartItems(cartItems);
            emptyCart.setCartItemPrices(cartItemPrices);
            return emptyCart;
        }
        else
        {
            return cart;
        }
    }
    public boolean isNear(LocationDAO loc1, LocationDAO loc2)
    {
        Double distance = Math.sqrt((Math.pow((loc1.getX() - loc2.getX()),2)) + (Math.pow((loc1.getY() - loc2.getY()),2)));
        return distance<=170;
    }


    public UserDAO getUser(Integer userId)
    {
        //TODO handle user doesn't exist
        return UserRepository.getInstance().getUserByID(userId);
    }

    public ArrayList<OrderDAO> getUserOrders(Integer UserId)
    {
        ArrayList<OrderDAO> orders = UserRepository.getInstance().getUserOrders(UserId);
        for(OrderDAO order:orders)
        {
            if(order.getStatus().toString().equals("Delivering"))
            {
                Date date = new Date();
                Timestamp now = new Timestamp(date.getTime());
                long diff = (now.getTime() - order.getlastAssignedTime().getTime())/1000;
                if(diff >= Math.round(order.getRemainingTime()))
                {
                    order.setStatus(OrderStatus.Done);
                    UserRepository.getInstance().setOrderStatusToDone(order.getId());
                }
            }
        }
        return orders;
    }

    public String createUser(String email, String firstName, String lastName , String password, String phoneNumber)  throws EmailAlreadyExistsException
    {
        if(UserRepository.getInstance().userExists(email))
        {
            throw new EmailAlreadyExistsException("There is an account with this email");
        }
        else
        {
            String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt());
            UserDAO newUser = new UserDAO(firstName, lastName, phoneNumber,email,0,new LocationDAO(0,0),hashedPassword);
            UserRepository.getInstance().createUser(newUser);
            return createToken(UserRepository.getInstance().getUserByEmail(email).getUserId());


        }
    }

    public LocationDAO getUserLocation(Integer userId)
    {
        return UserRepository.getInstance().getUserLocation(userId);
    }

    public String login(String email, String pwd) throws WrongUsernamePasswordException
    {
        if(UserRepository.getInstance().userExists(email))
        {
            UserDAO user = UserRepository.getInstance().getUserByEmail(email);
            if(BCrypt.checkpw(pwd, user.getPassword()))
            {
                return createToken(user.getUserId());
            }
            else
            {
                throw new WrongUsernamePasswordException("Wrong username or password!");
            }
        }
        else
        {
            throw new WrongUsernamePasswordException("Wrong username or password!");
        }
    }

    public String loginWithGoogle(String token) throws GeneralSecurityException, IOException, WrongUsernamePasswordException, NoJwtTokenProvidedException {
        JacksonFactory jacksonFactory = new JacksonFactory();
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), jacksonFactory)
                .setAudience(Collections.singletonList(CLIENT_ID))
                .build();
        GoogleIdToken idToken = verifier.verify(token);
        if (idToken != null) {
            Payload payload = idToken.getPayload();
            String email = payload.getEmail();
            if(UserRepository.getInstance().userExists(email))
            {
                UserDAO user = UserRepository.getInstance().getUserByEmail(email);
                return createToken(user.getUserId());
            }
            else {
                throw new WrongUsernamePasswordException("Wrong username or password!");
            }
        }
        else
        {
            throw new NoJwtTokenProvidedException("provided token is null");
        }
    }

}
