package Domain.FoodParty;


import Domain.Restaurant.RestaurantManager;
import Repository.FoodParty.FoodPartyRepository;
import Repository.FoodParty.PartyFoodDAO;
import Repository.Restaurant.FoodDAO;
import Repository.Restaurant.RestaurantDAO;
import Repository.Restaurant.RestaurantRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;


public class FoodPartyManager {
    private static FoodPartyManager instance;

    public static FoodPartyManager getInstance(){
        if(instance==null)
            instance = new FoodPartyManager();
        return instance;
    }

    public void setDeadline(){
        FoodPartyRepository.getInstance().setDeadline(LocalDateTime.now().plusMinutes(30).toString());
    }

    public void resetFoodParty(){
        FoodPartyRepository.getInstance().deleteAll();
        RestaurantRepository.getInstance().removeFoodsFromFoodParty();
    }

    public void updateFoodParty(ArrayList<RestaurantDAO> foodPartyRestaurants){
        RestaurantManager.getInstance().updateRestaurants(foodPartyRestaurants);
        int i = 0;
        for(RestaurantDAO restaurant: foodPartyRestaurants){
            for(FoodDAO food: restaurant.getMenu()){
                int foodId = RestaurantRepository.getInstance().getFoodId(restaurant.getId(),food.getName());
                FoodPartyRepository.getInstance().addPartyFood((PartyFoodDAO) food, foodId, i);
                i++;
            }
        }
    }

    public ArrayList<PartyFoodDAO> getPartyFoods(){
        return FoodPartyRepository.getInstance().getFoodPartyFoods();
    }

    public String getDeadline(){
        return FoodPartyRepository.getInstance().getDeadline();
    }


    public void incrementFoodPartyCount(String RestaurantId, int FoodId)
    {
        FoodPartyRepository.getInstance().incrementCount(RestaurantId,FoodId);
    }

    public void decrementFoodPartyCount(String RestaurantId, int FoodId)
    {
        FoodPartyRepository.getInstance().decrementCount(RestaurantId,FoodId);
    }

}
