package Domain.FoodParty;

import Repository.Restaurant.RestaurantDAO;
import Utilities.GetFromUrl;

import java.util.ArrayList;

public class GetFoodPartyJob implements Runnable{
    @Override
    public void run() {
        System.out.println("GetFoodParty JOB: Getting food party items from api.");
        String json = GetFromUrl.getStringFromURL("http://138.197.181.131:8080/foodparty");
        System.out.println("GetFoodParty JOB: " + json);
        FoodPartyManager.getInstance().resetFoodParty();
        ArrayList<RestaurantDAO> foodPartyRestaurants = CustomFoodPartyParse.map(json);
        FoodPartyManager.getInstance().updateFoodParty(foodPartyRestaurants);
        FoodPartyManager.getInstance().setDeadline();
    }
}
