package Domain.FoodParty;

import Repository.FoodParty.PartyFoodDAO;
import Repository.LocationDAO;
import Repository.Restaurant.FoodDAO;
import Repository.Restaurant.RestaurantDAO;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;

public class CustomFoodPartyParse {
    public static ArrayList<RestaurantDAO> map(String json) {

        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode nodes = mapper.readTree(json);
            ArrayList<RestaurantDAO> restaurants = new ArrayList<>();
            for (JsonNode restaurantNode : nodes) {
                RestaurantDAO restaurant = new RestaurantDAO();
                restaurant.setId(restaurantNode.get("id").asText());
                restaurant.setName(restaurantNode.get("name").asText());
                restaurant.setLogoURL(restaurantNode.get("logo").asText());
                restaurant.setLocation(new ObjectMapper().convertValue(restaurantNode.get("location"), LocationDAO.class));
                JsonNode menuNode = restaurantNode.get("menu");
                ArrayList<FoodDAO> menu = new ArrayList<>();
                for (JsonNode foodNode : menuNode) {
                    PartyFoodDAO food = new PartyFoodDAO();
                    food.setCount(foodNode.get("count").asInt());
                    food.setNewPrice(foodNode.get("price").asInt());
                    food.setName(foodNode.get("name").asText());
                    food.setDescription(foodNode.get("description").asText());
                    food.setPrice(foodNode.get("oldPrice").asInt());
                    food.setPopularity(foodNode.get("popularity").asDouble());
                    food.setImageURL(foodNode.get("image").asText());
                    food.setInFoodParty(true);
                    menu.add(food);
                }
                restaurant.setMenu(menu);
                restaurant.setSelfIdInMenuItems();
                restaurants.add(restaurant);
            }
            return restaurants;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
