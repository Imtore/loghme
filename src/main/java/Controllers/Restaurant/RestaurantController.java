package Controllers.Restaurant;

import Domain.Restaurant.RestaurantManager;
import Repository.Restaurant.RestaurantDAO;
import Exceptions.RestaurantNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;

@RestController
public class RestaurantController {

    //DONE!
    //TODO: add line to script for default user.
    @RequestMapping(value="/restaurants", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<RestaurantInfoDTO> getRestaurantsList(@RequestParam(value = "offset") int offset, @RequestParam(value = "count") int count){
        ArrayList<RestaurantDAO> restaurantDAOs = RestaurantManager.getInstance().getRestaurants(1,offset, count);
        ArrayList<RestaurantInfoDTO> restaurantDTOS = new ArrayList<>();
        for(RestaurantDAO restaurantDAO: restaurantDAOs){
            restaurantDTOS.add(new RestaurantInfoDTO(restaurantDAO));
        }
        return restaurantDTOS;
    }

    //DONE!
    @RequestMapping(value="/restaurants/{restaurantId}", method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public FullRestaurantDTO getRestaurantInfo(@PathVariable(value = "restaurantId") String restaurantId){
        try {
            RestaurantDAO restaurantDAO = RestaurantManager.getInstance().getRestaurantById(restaurantId);
            FullRestaurantDTO restaurantDTO = new FullRestaurantDTO(restaurantDAO);
            return restaurantDTO;
        }catch(RestaurantNotFoundException rnf){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Restaurant " + restaurantId + "was not found.", rnf);
        }
    }


    @RequestMapping(value="/restaurants/search", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<RestaurantInfoDTO> getChosenRestaurantInfo(@RequestParam(value = "restaurant") String restaurant, @RequestParam(value = "food") String food) {
            ArrayList<RestaurantDAO> restaurantDAOs = RestaurantManager.getInstance().search(food, restaurant);
            ArrayList<RestaurantInfoDTO> restaurantInfoDTOS = new ArrayList<>();
            for(RestaurantDAO restaurantDAO: restaurantDAOs){
                RestaurantInfoDTO restaurantDTO = new RestaurantInfoDTO(restaurantDAO);
                restaurantInfoDTOS.add(restaurantDTO);
            }
            return restaurantInfoDTOS;
    }
}
