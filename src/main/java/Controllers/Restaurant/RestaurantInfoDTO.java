package Controllers.Restaurant;

import Repository.LocationDAO;
import Repository.Restaurant.FoodDAO;
import Repository.Restaurant.RestaurantDAO;

import java.util.ArrayList;

public class RestaurantInfoDTO {
    String id;
    String name;
    String logoURL;

    RestaurantInfoDTO(RestaurantDAO restaurant){
        this.id=restaurant.getId();
        this.name=restaurant.getName();
        this.logoURL=restaurant.getLogoURL();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLogoURL() {
        return logoURL;
    }


}
