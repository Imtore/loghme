package Controllers.Restaurant;

import Repository.LocationDAO;
import Repository.Restaurant.FoodDAO;
import Repository.Restaurant.RestaurantDAO;
import com.fasterxml.jackson.annotation.JsonAlias;

import java.util.ArrayList;

public class FullRestaurantDTO {
    String id;
    String name;
    String logoURL;
    ArrayList<FoodDAO> menu = new ArrayList<>();

    FullRestaurantDTO(RestaurantDAO restaurantDAO){
        this.id=restaurantDAO.getId();
        this.name=restaurantDAO.getName();
        this.logoURL=restaurantDAO.getLogoURL();
        this.menu = restaurantDAO.getMenu();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLogoURL() {
        return logoURL;
    }

    public ArrayList<FoodDAO> getMenu() {
        return menu;
    }
}
