package Controllers.FoodParty;

import Repository.FoodParty.PartyFoodDAO;

import java.util.ArrayList;

public class FoodPartyDTO {
    String Deadline;
    ArrayList<PartyFoodDTO> partyFoods = new ArrayList<>();

    public String getDeadline() {
        return Deadline;
    }

    public ArrayList<PartyFoodDTO> getPartyFoods() {
        return partyFoods;
    }

    public void setPartyFoods(ArrayList<PartyFoodDAO> partyFoodDAOS) {
        for(PartyFoodDAO partyFoodDAO: partyFoodDAOS){
            this.partyFoods.add(new PartyFoodDTO(partyFoodDAO));
        }
    }

    public void setDeadline(String deadline) {
        Deadline = deadline;
    }
}
