package Controllers.FoodParty;

import Domain.FoodParty.FoodPartyManager;
import Repository.FoodParty.PartyFoodDAO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class FoodPartyController {
    @RequestMapping(value="/foodParty", method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
    public FoodPartyDTO getFoodPartyRestaurantsInfo(){
        FoodPartyDTO foodPartyDTO = new FoodPartyDTO();
        ArrayList<PartyFoodDAO> partyFoods = FoodPartyManager.getInstance().getPartyFoods();
        foodPartyDTO.setPartyFoods(partyFoods);
        foodPartyDTO.setDeadline(FoodPartyManager.getInstance().getDeadline());
        return foodPartyDTO;
    }

}
