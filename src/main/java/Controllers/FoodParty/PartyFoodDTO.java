package Controllers.FoodParty;

import Repository.FoodParty.PartyFoodDAO;
import com.fasterxml.jackson.annotation.JsonAlias;

public class PartyFoodDTO {
    Integer count;
    Integer newPrice;
    String restaurantName;
    String name;
    String description;
    String restaurantId;
    double popularity;
    Integer price;
    String imageURL;

    PartyFoodDTO(PartyFoodDAO partyFoodDAO){
        this.count = partyFoodDAO.getCount();
        this.newPrice = partyFoodDAO.getNewPrice();
        this.restaurantName = partyFoodDAO.getRestaurantName();
        this.name = partyFoodDAO.getName();
        this.description = partyFoodDAO.getDescription();
        this.restaurantId = partyFoodDAO.getRestaurantId();
        this.popularity = partyFoodDAO.getPopularity();
        this.price = partyFoodDAO.getPrice();
        this.imageURL = partyFoodDAO.getImageURL();
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public Integer getCount() {
        return count;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public String getImageURL() {
        return imageURL;
    }

    public Integer getPrice() {
        return price;
    }

    public double getPopularity() {
        return popularity;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public Integer getNewPrice() {
        return newPrice;
    }
}
