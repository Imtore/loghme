package Controllers.User;

import Controllers.User.CartDTO;
import Controllers.User.OrderDTO;
import Repository.User.OrderDAO;
import Repository.User.UserDAO;
import Repository.User.CartDAO;

import java.util.ArrayList;

public class UserDTO {
    Integer id;
    String firstName;
    String lastName;
    String phoneNumber;
    String email;
    Integer credit;
    CartDTO cart ;
    ArrayList<OrderDTO> orders;

    public UserDTO(UserDAO user, ArrayList<OrderDAO> orders, CartDAO cart)
    {
        this.id = user.getUserId();
        firstName = user.getFirstName();
        lastName = user.getLastName();
        phoneNumber = user.getPhoneNumber();
        email = user.getEmail();
        credit = user.getCredit();
        this.cart = new CartDTO(cart);
        this.orders = new ArrayList<>();
        Integer orderId = 0;
        for(OrderDAO order:orders)
        {
            this.orders.add(new OrderDTO(order,orderId));
            orderId+=1;
        }


    }

    public ArrayList<OrderDTO> getOrders() {
        return orders;
    }


    public CartDTO getCart() {
        return cart;
    }

    public Integer getId() {
        return id;
    }

    public Integer getCredit() {
        return credit;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}

