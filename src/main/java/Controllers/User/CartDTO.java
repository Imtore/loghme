package Controllers.User;

import Domain.Restaurant.RestaurantManager;
import Repository.User.CartDAO;
import Utilities.DeepCopy;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class CartDTO {
    String restaurantId="";
    String restaurantName="";
    HashMap<String, Integer> cartItems;
    HashMap<String, Integer> cartItemPrices;
    Boolean empty;
    Integer total;

    public CartDTO(CartDAO cart)
    {
        if(cart.getCartID()==null)
        {
            restaurantName = null;
            restaurantId = null;
            cartItems = new HashMap<String, Integer>();
            cartItemPrices = new HashMap<String, Integer>();
            empty = true;
            total = 0;

        }
        else {
            restaurantName = RestaurantManager.getInstance().getRestaurantName(cart.getRestaurantId());
            restaurantId = cart.getRestaurantId();
            cartItems = DeepCopy.copyMap(cart.getCartItems());
            cartItemPrices = DeepCopy.copyMap(cart.getCartItemPrices());
            empty = false;
            Integer total = 0;
            Iterator hmIterator = cart.getCartItems().entrySet().iterator();
            while (hmIterator.hasNext()) {
                Map.Entry mapElement = (Map.Entry)hmIterator.next();
                String foodName = (String)mapElement.getKey();
                Integer foodPrice = cartItemPrices.get(foodName);
                Integer count = (Integer) mapElement.getValue();
                total+=foodPrice*count;
            }
            this.total = total;
        }
    }

    public Boolean getEmpty() {
        return empty;
    }

    public HashMap<String, Integer> getCartItemPrices() {
        return cartItemPrices;
    }

    public HashMap<String, Integer> getCartItems() {
        return cartItems;
    }

    public Integer getTotal() {
        return total;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public String getRestaurantName() {
        return restaurantName;
    }
}
