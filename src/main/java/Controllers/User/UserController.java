package Controllers.User;

import Controllers.User.CartDTO;
import Controllers.User.UserDTO;
import Controllers.Interfaces.cartItem;
import Domain.User.UserManager;
import Repository.User.CartDAO;
import Repository.User.OrderDAO;
import Repository.User.UserDAO;
import Exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;


@RestController
public class UserController {

    @ExceptionHandler(NotEnoughCredit.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Object finalizeOrderValidationError1(NotEnoughCredit ex) {
        String result = ex.getMessage();
        System.out.println("finalizeOrderValidationError: "+result);
        return ex;
    }


    @ExceptionHandler(CartIsEmpty.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Object finalizeOrderValidationError2(CartIsEmpty ex) {
        String result = ex.getMessage();
        System.out.println("finalizeOrderValidationError: "+result);
        return ex;
    }




    @ExceptionHandler(AlreadyHaveItemsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Object addToCartValidationError(AlreadyHaveItemsException ex) {
        String result = ex.getMessage();
        System.out.println("addToCartValidationError: "+result);
        return ex;
    }




    @RequestMapping(value = "/users/orders", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void finalizeOrder(@RequestAttribute(value = "userId") String userId) {
        //TODO: this must change to findUserById later.
        try {
            UserManager.getInstance().finalizeOrder(Integer.parseInt(userId));
        }catch(CartIsEmpty cie){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Could not finalize order.", cie);

        }catch(NotEnoughCredit nec){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Could not finalize order. Not enough credit", nec);
        }
    }

    @RequestMapping(value = "/users/cart", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public CartDTO addToCart(@RequestAttribute(value = "userId") String userId,
                          @RequestBody cartItem item) {
        try {
            CartDAO modifiedCart = UserManager.getInstance().addToCart(Integer.parseInt(userId),item.foodName,item.restaurantId,item.count);
            return new CartDTO(modifiedCart);

        }catch(AlreadyHaveItemsException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Could not add to cart. There are already items from another restaurant.", e);
        }catch(RestaurantIsNotNearException rnn){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Restaurant does not follow policies. It is not near you", rnn);
        }catch(RestaurantNotFoundException rnf){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Restaurant not found", rnf);
        }

    }

    @RequestMapping(value = "/users/cart", method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public CartDTO deleteFromCart(@RequestAttribute(value = "userId") String userId,
                               @RequestBody cartItem item) {
        CartDAO modifiedCart = UserManager.getInstance().deleteFromCart(Integer.parseInt(userId),item.foodName,item.restaurantId);
        return new CartDTO(modifiedCart);
    }

    @RequestMapping(value = "/users/cart", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public CartDTO getCartInfo(@RequestAttribute(value = "userId") String userId) {
        CartDAO cart = UserManager.getInstance().getCart(Integer.parseInt(userId));
        return new CartDTO(cart);
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO getUserInfo(@RequestAttribute(value = "userId") String userId){
        //TODO: this must change to findUserById later.
        UserDAO user = UserManager.getInstance().getUser(Integer.parseInt(userId));
        ArrayList<OrderDAO> orders = UserManager.getInstance().getUserOrders(Integer.parseInt(userId));
        CartDAO cart = UserManager.getInstance().getCart(Integer.parseInt(userId));
        UserDTO userDto = new UserDTO(user,orders,cart);
        return userDto;
    }

    @RequestMapping(value="/users/credit", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Integer increaseCredit(@RequestAttribute(value = "userId") String userId,
                               @RequestParam(value = "credit") Integer credit){
        //TODO: this must change to findUserById later.
        Integer newCredit = UserManager.getInstance().IncreaseCredit(Integer.parseInt(userId),credit);
        return newCredit;
    }
}
