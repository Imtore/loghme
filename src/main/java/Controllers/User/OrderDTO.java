package Controllers.User;

import Controllers.User.CartDTO;
import Repository.User.OrderDAO;
import Repository.User.OrderStatus;

public class OrderDTO {
    private Integer id;
    private CartDTO cart ;
    private OrderStatus status;
//    private Delivery delivery;
//    private Double remainingTime;
//    private Timestamp AssignedTime;
    public OrderDTO(OrderDAO order,Integer id)
    {
        this.id = id;
        this.cart = new CartDTO(order.getCart());
        this.status = order.getStatus();
    }

    public Integer getId() {
        return id;
    }

    public CartDTO getCart() {
        return cart;
    }

    public OrderStatus getStatus() {
        return status;
    }

}
