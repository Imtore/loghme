package Controllers;

import Controllers.Interfaces.GoogleResponse;
import Controllers.Interfaces.createUserItem;
import Controllers.Interfaces.loginUserItem;
import Domain.User.UserManager;
import Exceptions.EmailAlreadyExistsException;
import Exceptions.NoJwtTokenProvidedException;
import Exceptions.WrongUsernamePasswordException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.security.GeneralSecurityException;


@RestController
public class AuthenticationController {

    @ExceptionHandler(EmailAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Object CreateUserValidationError(EmailAlreadyExistsException ex) {
        String result = ex.getMessage();
        System.out.println("signup validation error: "+result);
        return ex;
    }

    @ExceptionHandler(WrongUsernamePasswordException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public Object loginValidationError(WrongUsernamePasswordException ex) {
        String result = ex.getMessage();
        System.out.println("login validation error: "+result);
        return ex;
    }

    @RequestMapping(value = "/auth/signup", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String CreateUser(@RequestBody createUserItem user) {
        try {

            return UserManager.getInstance().createUser(user.email, user.firstName, user.lastName, user.password, user.phoneNumber);
        } catch (EmailAlreadyExistsException cie) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "There is an account with this email.", cie);

        }
    }

    @RequestMapping(value = "/auth/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String login(@RequestBody loginUserItem user) {

        try {
            return UserManager.getInstance().login(user.email,user.password);
        } catch (WrongUsernamePasswordException e) {
            throw new ResponseStatusException(
                    HttpStatus.FORBIDDEN, "wrong username or password was provided", e);
        }

    }

    @RequestMapping(value = "/auth/google", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String loginGoogle(@RequestBody GoogleResponse googleResponse) {

        try {
            return UserManager.getInstance().loginWithGoogle(googleResponse.token);
        } catch (GeneralSecurityException | IOException | NoJwtTokenProvidedException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED, "couldn't verify your token", e);
        }catch (WrongUsernamePasswordException e) {
            throw new ResponseStatusException(
                    HttpStatus.FORBIDDEN, "you haven't signed up yet", e);
        }

    }


}
