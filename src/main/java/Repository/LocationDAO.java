package Repository;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class LocationDAO {
    double x;
    double y;

    public LocationDAO(){

    }

    @JsonIgnore
    public LocationDAO(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }
}