package Repository.User;
import Repository.LocationDAO;

public class UserDAO {
    Integer id;
    String firstName;
    String lastName;
    String phoneNumber;
    String email;
    Integer credit;
    LocationDAO location;
//    Boolean islogIn;
    String password;

    public UserDAO( String firstName, String lastName, String phoneNumber, String email, Integer credit, LocationDAO location){
//        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.credit = credit;
        this.location = location;
        this.email = email;
//        this.islogIn = false;

    }

    public UserDAO( String firstName, String lastName, String phoneNumber, String email, Integer credit, LocationDAO location, String password){
//        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.credit = credit;
        this.location = location;
        this.password = password;
        this.email = email;
//        this.islogIn = false;

    }

    public UserDAO(Integer id ,String firstName, String lastName, String phoneNumber, String email, Integer credit, LocationDAO location, String password){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.credit = credit;
        this.location = location;
        this.email = email;
        this.password = password;

    }

    public String getPassword() {
        return password;
    }

    public Integer getCredit() {
        return credit;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail(){ return email; }

    public Integer getUserId() { return id;}


    public LocationDAO getLocation() {
        return location;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public void setId(Integer id)
    {
        this.id = id;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}
