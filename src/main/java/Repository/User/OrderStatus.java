package Repository.User;

public enum OrderStatus {
    FindingDelivery,
    Delivering,
    Done
}
