package Repository.User;
import java.sql.Time;
import java.util.Date;
import java.sql.Timestamp;

import Repository.DeliveryDAO;

public class OrderDAO {
    private Integer id;
    private CartDAO cart ;//= new Cart();
    private OrderStatus status;
    private DeliveryDAO delivery;
    private Double remainingTime;
    private  Timestamp AssignedTime;


    public OrderDAO()
    {}

    public Timestamp getlastAssignedTime() {
        return AssignedTime;
    }

    public Double getRemainingTime() {
        return remainingTime;
    }


    public Integer getId() {
        return id;
    }


    public CartDAO getCart() {
        return cart;
    }

    public DeliveryDAO getDelivery() {
        return delivery;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setRemainingTime(Double remainingTime) {
        this.remainingTime = remainingTime;
    }

    public void setAssignedTime(Timestamp AssignedTime) {
        this.AssignedTime = AssignedTime;
    }

    public void setCart(CartDAO cart) {
        this.cart = cart;
    }

    public void setDelivery(DeliveryDAO delivery) {
        this.delivery = delivery;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }
}
