package Repository.User;

import Utilities.DeepCopy;

import java.util.HashMap;

public class CartDAO {
    Integer cartID;
    Integer userId;
    String restaurantId="";
    HashMap<String, Integer> cartItems ;//= new HashMap<String, Integer>();
    HashMap<String, Integer> cartItemPrices;

//    public CartDAO(CartDAO cart){
//        userId = cart.userId;
//        restaurantId = cart.getRestaurantId();
//        cartItems = DeepCopy.copyMap(cart.getCartItems());
//    }

    public CartDAO()
    {}

//    public CartDAO(String restId, String userId, HashMap<String,Integer> cartItems, HashMap<String,Integer> cartItemPrices)
//    {
//        this.restaurantId = restId;
//        this.userId = userId;
//        this.cartItemPrices = cartItemPrices;
//        this.cartItems = cartItems;
//    }

    public String getRestaurantId() {
        return restaurantId;
    }



    public HashMap<String, Integer> getCartItems() {
        return cartItems;
    }

    public HashMap<String, Integer> getCartItemPrices() {
        return cartItemPrices;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCartID() {
        return cartID;
    }

    public void setCartID(Integer cartID) {
        this.cartID = cartID;
    }

    public void setCartItemPrices(HashMap<String, Integer> cartItemPrices) {
        this.cartItemPrices = DeepCopy.copyMap(cartItemPrices);
    }

    public void setCartItems(HashMap<String, Integer> cartItems) {
        this.cartItems = DeepCopy.copyMap(cartItems);
    }
}
