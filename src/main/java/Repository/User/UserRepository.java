package Repository.User;

import Repository.FoodParty.FoodPartyRepository;
import Repository.LocationDAO;
import Repository.ConnectionPool;
import Repository.Restaurant.RestaurantRepository;
import Utilities.DateTimeConverter;


import java.time.LocalDateTime;
import java.util.Date;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class UserRepository {

    private static UserRepository instance;

    public static UserRepository getInstance(){
        if(instance==null)
            instance = new UserRepository();
        return instance;
    }

    public void createUser(UserDAO newUser)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO User(FirstName, LastName, PhoneNumber, Email, Credit, X, Y, password) values (?, ?, ?, ?, ?, ?, ?, ?) "
                            + "");
            statement.setString(1, newUser.getFirstName());
            statement.setString(2, newUser.getLastName());
            statement.setString(3, newUser.getPhoneNumber());
            statement.setString(4, newUser.getEmail());
            statement.setInt(5,newUser.getCredit());
            statement.setDouble(6,newUser.getLocation().getX());
            statement.setDouble(7, newUser.getLocation().getY());
            statement.setString(8, newUser.getPassword());
            statement.executeUpdate();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public UserDAO getUserByEmail(String email)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "select UserID,FirstName, LastName, PhoneNumber, Email, Credit, X, Y, password from User where "
                            + "Email = ?");
            statement.setString(1, email);
            ResultSet result = statement.executeQuery();
            if(result.next())
            {
                UserDAO user = new UserDAO(result.getInt("UserID"),result.getString("FirstName"),result.getString("LastName"),
                        result.getString("PhoneNumber"),email,result.getInt("Credit"),
                        new LocationDAO(result.getDouble("X"), result.getDouble("Y")),result.getString("password"));
                result.close();
                statement.close();
                connection.close();
                return user;
            }
            else
            {
                result.close();
                statement.close();
                connection.close();
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }


    public UserDAO getUserByID(Integer userId)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "select UserID,FirstName, LastName, PhoneNumber, Email, Credit, X, Y, password from User where "
                            + "UserID = ?");
            statement.setInt(1, userId);
            ResultSet result = statement.executeQuery();
            if(result.next())
            {
                UserDAO user = new UserDAO(result.getInt("UserID"),result.getString("FirstName"),result.getString("LastName"),
                        result.getString("PhoneNumber"),result.getString("Email"),result.getInt("Credit"),
                        new LocationDAO(result.getDouble("X"), result.getDouble("Y")),result.getString("password"));
                result.close();
                statement.close();
                connection.close();
                return user;
            }
            else
            {
                result.close();
                statement.close();
                connection.close();
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    public CartDAO getUserNotClosedCart(Integer userId)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "select CartID, RestaurantID from Cart where "
                            + "UserID = ? and IsClosed=0");
            statement.setInt(1, userId);
            ResultSet result = statement.executeQuery();
            if(result.next())
            {
                Integer cartId = result.getInt("CartID");
                CartDAO cart  = new CartDAO();
                cart.setRestaurantId(result.getString("RestaurantID"));
                cart.setCartID(cartId);
                cart.setUserId(userId);
                result.close();
                statement.close();
                connection.close();
                HashMap <String,Integer> cartItems =new HashMap<String, Integer>();
                HashMap <String,Integer> cartItemPrices =new HashMap<String, Integer>();
                Connection connection2 = ConnectionPool.getInstance().getConnection();
                PreparedStatement statement2 = connection2.prepareStatement(
                        "select F.Name,CF.Count, F.Price, F.IsInFoodParty from Food F inner join CartFood CF on F.FoodID = CF.FoodID where "
                                + "CF.CartID = ? and CF.RestaurantID = F.RestaurantID");
                statement2.setInt(1, cartId);
                result = statement2.executeQuery();
                while(result.next())
                {
                    String FoodName = result.getString("Name");
                    cartItems.put(FoodName, result.getInt("Count"));
                    Integer foodId = RestaurantRepository.getInstance().getFoodId(cart.getRestaurantId(),FoodName);
                    if(RestaurantRepository.getInstance().isInFoodParty(foodId,cart.getRestaurantId()))
                    {
                        cartItemPrices.put(FoodName, FoodPartyRepository.getInstance().getFoodPartyPrice(cart.getRestaurantId(),foodId));
                    }
                    else {
                        cartItemPrices.put(FoodName, result.getInt("Price"));
                    }
                }
                cart.setCartItemPrices(cartItemPrices);
                cart.setCartItems(cartItems);
                result.close();
                statement2.close();
                connection2.close();
                return cart;
            }
            else
            {
                result.close();
                statement.close();
                connection.close();
                return null;
            }



        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


    public CartDAO getCart(Integer cartId)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "select UserID, RestaurantID from Cart where "
                            + "CartID = ?");
            statement.setInt(1, cartId);
            ResultSet result = statement.executeQuery();
            CartDAO cart  = new CartDAO();
            result.next();
            cart.setRestaurantId(result.getString("RestaurantID"));
            cart.setCartID(cartId);
            cart.setUserId(result.getInt("UserID"));
            result.close();
            statement.close();
            connection.close();
            HashMap <String,Integer> cartItems =new HashMap<String, Integer>();
            HashMap <String,Integer> cartItemPrices =new HashMap<String, Integer>();
            Connection connection2 = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement2 = connection2.prepareStatement(
                    "select F.Name,CF.Count, F.Price, F.FoodID from Food F, CartFood CF where "
                            + "CF.CartID = ? and CF.FoodID = F.FoodID and CF.RestaurantID = F.RestaurantID");
            statement2.setInt(1, cartId);
            ResultSet result2 = statement2.executeQuery();
            while(result2.next())
            {
                String FoodName = result2.getString("Name");
                cartItems.put(FoodName, result2.getInt("Count"));
                Integer foodId = result2.getInt("FoodID");
                if(RestaurantRepository.getInstance().isInFoodParty(foodId,cart.getRestaurantId()))
                {
                    cartItemPrices.put(FoodName, FoodPartyRepository.getInstance().getFoodPartyPrice(cart.getRestaurantId(),foodId));
                }
                else {
                    cartItemPrices.put(FoodName, result2.getInt("Price"));
                }
            }
            cart.setCartItemPrices(cartItemPrices);
            cart.setCartItems(cartItems);
            result2.close();
            statement2.close();
            connection2.close();
            return cart;



        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }


    public ArrayList<OrderDAO> getUserOrders(Integer UserId)
    {
        ArrayList<OrderDAO> orders = new ArrayList<>();
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "select O.OrderID, O.CartID, O.Status,O.RemainingTime, O.LastAssignedTime from `Order` O inner join Cart C on O.CartID=C.CartID where "
                            + "C.UserID = ?");
            statement.setInt(1, UserId);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                OrderDAO order = new OrderDAO();
                order.setId(result.getInt("OrderID"));
                order.setCart(this.getCart(result.getInt("CartID")));
                order.setStatus(OrderStatus.valueOf(result.getString("Status")));
                order.setRemainingTime(result.getDouble("RemainingTime"));
                order.setAssignedTime(result.getTimestamp("LastAssignedTime"));
                orders.add(order);
            }
            result.close();
            statement.close();
            connection.close();
            return orders;
        }catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
    }


    public ArrayList<OrderDAO> getNotAssignedOrders()
    {
        ArrayList<OrderDAO> orders = new ArrayList<>();
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "select O.OrderID, C.UserID ,O.CartID, O.Status,O.RemainingTime, O.LastAssignedTime from `Order` O inner join Cart C on O.CartID=C.CartID where " +
                            "O.Status = \"FindingDelivery\"");

            ResultSet result = statement.executeQuery();
            while (result.next()) {
                OrderDAO order = new OrderDAO();
                order.setId(result.getInt("OrderID"));
                order.setCart(this.getCart(result.getInt("CartID")));
                order.setStatus(OrderStatus.valueOf(result.getString("Status")));
//                String deadline = DateTimeConverter.SQLToLocalDateTime(result.getString("Deadline"));
                order.setRemainingTime(result.getDouble("RemainingTime"));
                order.setAssignedTime(result.getTimestamp("LastAssignedTime"));
                orders.add(order);
            }
            result.close();
            statement.close();
            connection.close();
            return orders;
        }catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setCredit(Integer userId, Integer newCredit)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "update User set Credit=? where UserID=? ");
            statement.setInt(1, newCredit);
            statement.setInt(2, userId);
            statement.executeUpdate();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void addToCart(Integer cartId, Integer FoodID, String RestID)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO CartFood (CartID, FoodID, RestaurantID, `Count`) VALUES (?,?,?,1)" +
                            "  ON DUPLICATE KEY UPDATE `Count`=`Count`+1;");
            statement.setInt(1, cartId);
            statement.setInt(2, FoodID);
            statement.setString(3, RestID);
            statement.executeUpdate();
            statement.close();
            connection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public CartDAO createCart(Integer UserId, String RestID)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO Cart (RestaurantID, UserId, IsClosed) VALUES (?,?,0)");
            statement.setString(1, RestID);
            statement.setInt(2, UserId);
            statement.executeUpdate();
            statement.close();
            connection.close();
            return this.getUserNotClosedCart(UserId);


        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    public Boolean cartIsEmpty(Integer cartId)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "select count(*) as items from CartFood WHERE CartID = ? ;");
            statement.setInt(1, cartId);
            ResultSet result = statement.executeQuery();
            result.next();
            Integer count = result.getInt("items");
            statement.close();
            connection.close();
            result.close();
            return count==0;


        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


    public void removeCart(Integer cartId)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement DeleteStatement = connection.prepareStatement(
                    "DELETE FROM Cart WHERE CartId = ? ;");
            DeleteStatement.setInt(1, cartId);
            DeleteStatement.executeUpdate();
            DeleteStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void RemoveFromCart(Integer cartId, Integer foodId)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement DeleteStatement = connection.prepareStatement(
                    "DELETE FROM CartFood WHERE CartId = ? and FoodID = ? AND `Count` = 1;");
            DeleteStatement.setInt(1, cartId);
            DeleteStatement.setInt(2, foodId);
            DeleteStatement.executeUpdate();
            DeleteStatement.close();
            connection.close();

            Connection connection2 = ConnectionPool.getInstance().getConnection();
            PreparedStatement DecrementStatement = connection2.prepareStatement(
                    "update CartFood set `Count` = `Count` - 1 WHERE CartId = ? and FoodID = ? ;");
            DecrementStatement.setInt(1, cartId);
            DecrementStatement.setInt(2, foodId);
            DecrementStatement.executeUpdate();
            DecrementStatement.close();
            connection2.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void CreateOrder(Integer CartId)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO `Order` (CartID, Status) VALUES (?,'FindingDelivery')");
            statement.setInt(1, CartId);
            statement.executeUpdate();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setOrderStatusToDelivering(Integer OrderId, Double RemainingTime)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "update `Order` set Status = 'Delivering' , RemainingTime = ? , LastAssignedTime = ? WHERE OrderId = ? ;");
            statement.setDouble(1, RemainingTime);
            Date date = new Date();
            statement.setTimestamp(2, new Timestamp(date.getTime()));
//            statement.setString(2, DateTimeConverter.localDateTimeToSQL(LocalDateTime.now().toString()));
            statement.setInt(3,OrderId);
            statement.executeUpdate();
            statement.close();
            connection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void setOrderStatusToDone(Integer OrderId)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "update `Order` set Status = 'Done' , RemainingTime = 0 WHERE OrderId = ? ;");
            statement.setInt(1,OrderId);
            statement.executeUpdate();
            statement.close();
            connection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public void CloseCart(Integer CartID)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "update Cart set IsClosed = 1  WHERE CartID = ? ;");
            statement.setInt(1,CartID);
            statement.executeUpdate();
            statement.close();
            connection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public LocationDAO getUserLocation(Integer UserId) {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "select  X, Y from User where "
                            + "UserID = ?");
            statement.setInt(1, UserId);
            ResultSet result = statement.executeQuery();
            result.next();
            LocationDAO loc =  new LocationDAO(result.getDouble("X"), result.getDouble("Y"));
            result.close();
            statement.close();
            connection.close();
            return loc;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Integer getUserCredit(Integer UserId)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "select  Credit from User where "
                            + "UserID = ?");
            statement.setInt(1, UserId);
            ResultSet result = statement.executeQuery();
            result.next();
            Integer credit = result.getInt("Credit");
            result.close();
            statement.close();
            connection.close();
            return credit;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Boolean userExists(String email)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "select UserID from User where "
                            + "Email = ?");
            statement.setString(1, email);
            ResultSet result = statement.executeQuery();
            Boolean userExists = null;
            if(result.next())
            {
                userExists = true;
            }
            else
            {
                userExists = false;
            }
            result.close();
            statement.close();
            connection.close();
            return userExists;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

}
