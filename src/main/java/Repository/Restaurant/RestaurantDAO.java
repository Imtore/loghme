package Repository.Restaurant;

import Repository.LocationDAO;
import com.fasterxml.jackson.annotation.JsonAlias;

import java.util.ArrayList;

public class RestaurantDAO {
    String id;
    String name;
    LocationDAO location;
    @JsonAlias("logo")
    String logoURL;
    ArrayList<FoodDAO> menu = new ArrayList<FoodDAO>();

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocationDAO getLocation() {
        return location;
    }

    public String getLogoURL() {
        return logoURL;
    }

    public ArrayList<FoodDAO> getMenu() {
        return menu;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocation(LocationDAO location) {
        this.location = location;
    }

    public void setLogoURL(String logoURL) {
        this.logoURL = logoURL;
    }

    public void setMenu(ArrayList<FoodDAO> menu) {
        this.menu = menu;
    }

    public void setSelfIdInMenuItems(){
        for(FoodDAO food: this.menu){
            food.setRestaurantId(this.id);
        }
    }

    public void addFood(FoodDAO food){
        this.menu.add(food);
    }
}
