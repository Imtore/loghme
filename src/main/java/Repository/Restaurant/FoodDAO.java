package Repository.Restaurant;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FoodDAO {
    @JsonAlias("foodName")
    String name;
    String description;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    String restaurantId;
    double popularity;
    Integer price;
    @JsonAlias("image")
    String imageURL;
    boolean isInFoodParty;


    public String getRestaurantId() {
        return restaurantId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public double getPopularity() {
        return popularity;
    }

    public Integer getPrice() {
        return price;
    }

    public String getImageURL() {
        return imageURL;
    }

    public boolean getIsInFoodParty(){
        return isInFoodParty;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public void setInFoodParty(boolean inFoodParty) {
        isInFoodParty = inFoodParty;
    }
}



