package Repository.Restaurant;

import Repository.LocationDAO;
import Repository.ConnectionPool;
import com.mysql.cj.protocol.Resultset;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class RestaurantRepository {
    private static RestaurantRepository instance;

    public static RestaurantRepository getInstance(){
        if(instance==null)
            instance = new RestaurantRepository();
        return instance;
    }

    public void addRestaurant(RestaurantDAO newRestaurant){
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO Restaurant values (?, ?, ?, ?, ?) "
                            + "");
            statement.setString(1, newRestaurant.getId());
            statement.setString(2, newRestaurant.getName());
            statement.setDouble(3, newRestaurant.getLocation().getX());
            statement.setDouble(4, newRestaurant.getLocation().getY());
            statement.setString(5, newRestaurant.getLogoURL());
            statement.executeUpdate();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<RestaurantDAO> getNearRestaurants(LocationDAO center, Integer offset, Integer count){
        try {
            ArrayList<RestaurantDAO> nearRestaurants = new ArrayList<RestaurantDAO>();
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Restaurant WHERE power(X-?,2)+power(Y-?,2)<=power(170,2) Order By RestaurantID LIMIT ?,?");
            statement.setDouble(1, center.getX());
            statement.setDouble(2, center.getY());
            statement.setInt(3, offset);
            statement.setInt(4, count);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                RestaurantDAO restaurantDAO = new RestaurantDAO();
                restaurantDAO.setId(result.getString("RestaurantID"));
                restaurantDAO.setName(result.getString("Name"));
                restaurantDAO.setLocation(new LocationDAO(result.getDouble("X"),result.getDouble("Y")));
                restaurantDAO.setLogoURL(result.getString("LogoURL"));
                nearRestaurants.add(restaurantDAO);
            }
            result.close();
            statement.close();
            connection.close();
            return nearRestaurants;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


    public void addRestaurantMenu(String restaurantId, ArrayList<FoodDAO> menu){
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO Food values (?, ?, ?, ?, ?, ?, ?, ?) ");
            for(FoodDAO food: menu){
                statement.clearParameters();
                statement.setInt(1, menu.indexOf(food));
                statement.setString(2, restaurantId);
                statement.setString(3, food.getName());
                statement.setString(4, food.getDescription());
                statement.setDouble(5, food.getPopularity());
                statement.setInt(6, food.getPrice());
                statement.setString(7, food.imageURL);
                statement.setBoolean(8, food.getIsInFoodParty());
                statement.addBatch();
            }
            statement.executeBatch();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public boolean restaurantExists(String restaurantId){
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Restaurant WHERE RestaurantID=?");
            statement.setString(1,restaurantId);
            ResultSet result = statement.executeQuery();
            if(result.next()){
                result.close();
                statement.close();
                connection.close();
                return true;
            }
            else{
                result.close();
                statement.close();
                connection.close();
                return false;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean foodExists(String restaurantId, String foodName){
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Food WHERE RestaurantID=? and `Name`=?");
            statement.setString(1,restaurantId);
            statement.setString(2, foodName);
            ResultSet result = statement.executeQuery();
            if(result.next()){
                result.close();
                statement.close();
                connection.close();
                return true;
            }
            result.close();
            statement.close();
            connection.close();
            return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public RestaurantDAO getFullRestaurantById(String restaurantId){
        try {
            RestaurantDAO restaurantDAO = new RestaurantDAO();
            ArrayList<FoodDAO> menu = new ArrayList<>();
            Connection connection = ConnectionPool.getInstance().getConnection();

            PreparedStatement statement1 = connection.prepareStatement(
                    "SELECT * FROM Restaurant WHERE RestaurantID=? ");
            statement1.setString(1, restaurantId);
            ResultSet restaurantResult = statement1.executeQuery();
            restaurantResult.next();
            restaurantDAO.setId(restaurantResult.getString("RestaurantID"));
            restaurantDAO.setName(restaurantResult.getString("Name"));
            restaurantDAO.setLocation(new LocationDAO(restaurantResult.getDouble("X"),restaurantResult.getDouble("Y")));
            restaurantDAO.setLogoURL(restaurantResult.getString("LogoURL"));
            restaurantResult.close();
            statement1.close();

            PreparedStatement statement2 = connection.prepareStatement(
            "SELECT * FROM Food WHERE RestaurantID=?");
            statement2.setString(1, restaurantId);
            ResultSet menuResult = statement2.executeQuery();
            while(menuResult.next()){
                FoodDAO foodDAO = new FoodDAO();
                foodDAO.setRestaurantId(menuResult.getString("RestaurantID"));
                foodDAO.setDescription(menuResult.getString("Description"));
                foodDAO.setName(menuResult.getString("Name"));
                foodDAO.setImageURL(menuResult.getString("ImageURL"));
                foodDAO.setPopularity(menuResult.getDouble("Popularity"));
                foodDAO.setPrice(menuResult.getInt("Price"));
                foodDAO.setInFoodParty(menuResult.getBoolean("IsInFoodParty"));
                menu.add(foodDAO);
            }
            restaurantDAO.setMenu(menu);
            menuResult.close();
            statement2.close();
            connection.close();
            return restaurantDAO;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public LocationDAO getRestaurantLocation(String restId)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT X,Y FROM Restaurant WHERE RestaurantID = ?");
            statement.setString(1, restId);
            ResultSet result = statement.executeQuery();
            if(result.next())
            {
                LocationDAO loc = new LocationDAO(result.getDouble("X"),result.getDouble("Y"));
                result.close();
                statement.close();
                connection.close();
                return loc;
            }
            else
            {
                result.close();
                statement.close();
                connection.close();
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


    public String getRestaurantName(String restId)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT Name FROM Restaurant WHERE RestaurantID = ?");
            statement.setString(1, restId);
            ResultSet result = statement.executeQuery();
            if(result.next())
            {
                String name = result.getString("Name");
                result.close();
                statement.close();
                connection.close();
                return name;
            }
            else
            {
                result.close();
                statement.close();
                connection.close();
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Integer getFoodId(String restId, String foodName)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT FoodID FROM Food WHERE RestaurantID = ? and Name = ?");
            statement.setString(1, restId);
            statement.setString(2, foodName);
            ResultSet result = statement.executeQuery();
            result.next();
            Integer foodId = result.getInt("FoodID");
            result.close();
            statement.close();
            connection.close();
            return foodId;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void removeFoodsFromFoodParty(){
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE Food SET IsInFoodParty = ? WHERE IsInFoodParty=?");
            statement.setBoolean(1, false);
            statement.setBoolean(2, true);
            statement.executeUpdate();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getMenuSize(String restaurantId){
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT count(*) FROM Food WHERE RestaurantID=?");
            statement.setString(1,restaurantId);
            ResultSet result = statement.executeQuery();
            int count = 0;
            if(result.next()){
                count = result.getInt("count(*)");
            }
            result.close();
            statement.close();
            connection.close();
            return count;

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void addFood(String restaurantId, int foodId, FoodDAO foodDAO){
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO Food values (?, ?, ?, ?, ?, ?, ?, ?) ");

            statement.setInt(1, foodId);
            statement.setString(2, restaurantId);
            statement.setString(3, foodDAO.getName());
            statement.setString(4, foodDAO.getDescription());
            statement.setDouble(5, foodDAO.getPopularity());
            statement.setInt(6, foodDAO.getPrice());
            statement.setString(7, foodDAO.imageURL);
            statement.setBoolean(8, foodDAO.getIsInFoodParty());
            statement.executeUpdate();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateFood(String restaurantId, int foodId, FoodDAO foodDAO){
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE Food SET IsInFoodParty = ?, description=?, popularity=?, price=?, imageURL=? WHERE RestaurantID=? and FoodID=?");
            statement.setBoolean(1, foodDAO.getIsInFoodParty());
            statement.setString(2, foodDAO.getDescription());
            statement.setDouble(3, foodDAO.getPopularity());
            statement.setInt(4, foodDAO.getPrice());
            statement.setString(5, foodDAO.imageURL);
            statement.setString(6, restaurantId);
            statement.setInt(7, foodId);
            statement.executeUpdate();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Boolean isInFoodParty(Integer foodId, String restID)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT IsInFoodParty FROM Food WHERE RestaurantID = ? and FoodID = ?");
            statement.setString(1, restID);
            statement.setInt(2, foodId);
            ResultSet result = statement.executeQuery();
            result.next();
            Boolean isInFoodparty = result.getBoolean("IsInFoodParty");
            result.close();
            statement.close();
            connection.close();
            return isInFoodparty;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


    public ArrayList<RestaurantDAO> searchForFoodInAll(String foodName){
        try {
            ArrayList<RestaurantDAO> restaurants = new ArrayList<>();
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement1 = connection.prepareStatement(
                    "SELECT * FROM Food WHERE `Name` LIKE ? ");
            PreparedStatement statement2 = connection.prepareStatement(
                    "SELECT * FROM Restaurant WHERE RestaurantID=? ");
            statement1.setString(1, '%'+foodName+'%');
            ResultSet foodResult = statement1.executeQuery();
            String currentRestaurantId="";
            RestaurantDAO currentRestaurant = null;
            while(foodResult.next()){
                String restaurantId = foodResult.getString("RestaurantID");
                if(!restaurantId.equals(currentRestaurantId)){
                    if(currentRestaurant!=null){ restaurants.add(currentRestaurant); }
                    currentRestaurant = new RestaurantDAO();
                    currentRestaurantId = restaurantId;
                    statement2.setString(1, currentRestaurantId);
                    ResultSet restaurantResult = statement2.executeQuery();
                    restaurantResult.next();
                    currentRestaurant.setId(restaurantResult.getString("RestaurantID"));
                    currentRestaurant.setName(restaurantResult.getString("Name"));
                    currentRestaurant.setLocation(new LocationDAO(restaurantResult.getDouble("X"),restaurantResult.getDouble("Y")));
                    currentRestaurant.setLogoURL(restaurantResult.getString("LogoURL"));
                    restaurantResult.close();
                }
//                FoodDAO food = new FoodDAO();
//                food.setRestaurantId(foodResult.getString("RestaurantID"));
//                food.setDescription(foodResult.getString("Description"));
//                food.setName(foodResult.getString("Name"));
//                food.setImageURL(foodResult.getString("ImageURL"));
//                food.setPopularity(foodResult.getDouble("Popularity"));
//                food.setPrice(foodResult.getInt("Price"));
//                food.setInFoodParty(foodResult.getBoolean("IsInFoodParty"));
//                currentRestaurant.addFood(food);
            }
            statement1.close();
            statement2.close();
            connection.close();
            return restaurants;
        }catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<RestaurantDAO> searchForFoodInRestaurant(String foodName, String restaurantName){
        try{
            ArrayList<RestaurantDAO> restaurants = new ArrayList<>();
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement1 = connection.prepareStatement(
                    "SELECT * FROM Restaurant WHERE `Name` LIKE ? ");
            PreparedStatement statement2 = connection.prepareStatement(
                    "SELECT * FROM Food WHERE RestaurantID=? AND `Name` LIKE ?");

            statement1.setString(1, '%'+restaurantName+'%');
            ResultSet restaurantResult = statement1.executeQuery();

            while(restaurantResult.next()){
                RestaurantDAO restaurant = new RestaurantDAO();
                boolean hadResult = false;

                statement2.setString(1,restaurantResult.getString("RestaurantID"));
                statement2.setString(2, '%'+foodName+'%');
                ResultSet foodResult = statement2.executeQuery();

                if(foodResult.next()){
                    hadResult = true;
//                    FoodDAO food = new FoodDAO();
//                    food.setRestaurantId(foodResult.getString("RestaurantID"));
//                    food.setDescription(foodResult.getString("Description"));
//                    food.setName(foodResult.getString("Name"));
//                    food.setImageURL(foodResult.getString("ImageURL"));
//                    food.setPopularity(foodResult.getDouble("Popularity"));
//                    food.setPrice(foodResult.getInt("Price"));
//                    food.setInFoodParty(foodResult.getBoolean("IsInFoodParty"));
//                    restaurant.addFood(food);
                }
                if(hadResult){
                    restaurant.setId(restaurantResult.getString("RestaurantID"));
                    restaurant.setName(restaurantResult.getString("Name"));
                    restaurant.setLocation(new LocationDAO(restaurantResult.getDouble("X"),restaurantResult.getDouble("Y")));
                    restaurant.setLogoURL(restaurantResult.getString("LogoURL"));
                    restaurants.add(restaurant);
                }

                foodResult.close();
            }
            restaurantResult.close();
            statement1.close();
            statement2.close();
            connection.close();
            return restaurants;
        }catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<RestaurantDAO> searchForRestaurant(String restaurantName){
        try {
            ArrayList<RestaurantDAO> restaurants = new ArrayList<>();
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Restaurant WHERE `Name` LIKE ? ");
            statement.setString(1, '%'+restaurantName+'%');
            ResultSet result = statement.executeQuery();
            while(result.next()){
                RestaurantDAO restaurant = new RestaurantDAO();
                restaurant.setId(result.getString("RestaurantID"));
                restaurant.setName(result.getString("Name"));
                restaurant.setLocation(new LocationDAO(result.getDouble("X"),result.getDouble("Y")));
                restaurant.setLogoURL(result.getString("LogoURL"));
                restaurants.add(restaurant);
            }
            result.close();
            statement.close();
            connection.close();
            return restaurants;
        }catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }

}
