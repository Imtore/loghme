package Repository;


public class DeliveryDAO {
    private String id;
    private Double velocity;
    private LocationDAO location;

    public void setId(String id) {
        this.id = id;
    }

    public void setLocation(LocationDAO location) {
        this.location = location;
    }

    public void setVelocity(Double velocity) {
        this.velocity = velocity;
    }

    public Double getVelocity() {
        return velocity;
    }

    public LocationDAO getLocation() {
        return location;
    }

    public String getId() {
        return id;
    }

}
