package Repository.FoodParty;
import Repository.Restaurant.FoodDAO;

public class PartyFoodDAO extends FoodDAO{
    Integer count;
    Integer newPrice;
    String restaurantName;

    public Integer getNewPrice() {
        return newPrice;
    }

    public Integer getCount() {
        return count;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setNewPrice(Integer newPrice) {
        this.newPrice = newPrice;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }
}
