package Repository.FoodParty;

import Repository.ConnectionPool;
import Utilities.DateTimeConverter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class FoodPartyRepository {
    private static FoodPartyRepository instance;

    public static FoodPartyRepository getInstance(){
        if(instance==null)
            instance = new FoodPartyRepository();
        return instance;
    }

    public void setDeadline(String deadline){
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE FoodParty SET Deadline = ? WHERE FID = 1");
            statement.setString(1, DateTimeConverter.localDateTimeToSQL(deadline));
            statement.executeUpdate();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll(){
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM PartyFood");
            statement.executeUpdate();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addPartyFood(PartyFoodDAO partyFood, int foodId, int id){
        try{
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO PartyFood( FoodID, Price, Count, RestaurantID) values(?, ?, ?, ?)");
//            statement.setInt(1,id);
            statement.setInt(1, foodId);
            statement.setInt(2, partyFood.getNewPrice());
            statement.setInt(3, partyFood.getCount());
            statement.setString(4, partyFood.getRestaurantId());
            statement.executeUpdate();
            statement.close();
            connection.close();

        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public ArrayList<PartyFoodDAO> getFoodPartyFoods(){
        try {
            ArrayList<PartyFoodDAO> foodPartyMenu = new ArrayList<>();
            Connection connection = ConnectionPool.getInstance().getConnection();

            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM PartyFood");
            ResultSet result = statement.executeQuery();

            PreparedStatement innerStatement1 = connection.prepareStatement(
                    "SELECT * FROM Food Where RestaurantID=? and FoodID=?"
            );

            PreparedStatement innerStatement2 = connection.prepareStatement(
                    "SELECT `Name` FROM Restaurant Where RestaurantID=?"
            );

            while(result.next()){
                PartyFoodDAO partyFoodDAO = new PartyFoodDAO();
                partyFoodDAO.setRestaurantId(result.getString("RestaurantID"));
                partyFoodDAO.setCount(result.getInt("Count"));
                partyFoodDAO.setNewPrice(result.getInt("Price"));

                innerStatement1.setString(1, result.getString("RestaurantID"));
                innerStatement1.setInt(2, result.getInt("FoodID"));
                ResultSet innerResult1 = innerStatement1.executeQuery();
                innerResult1.next();
                partyFoodDAO.setDescription(innerResult1.getString("Description"));
                partyFoodDAO.setName(innerResult1.getString("Name"));
                partyFoodDAO.setImageURL(innerResult1.getString("ImageURL"));
                partyFoodDAO.setPopularity(innerResult1.getDouble("Popularity"));
                partyFoodDAO.setPrice(innerResult1.getInt("Price"));
                partyFoodDAO.setInFoodParty(innerResult1.getBoolean("IsInFoodParty"));

                innerStatement2.setString(1, result.getString("RestaurantID"));
                ResultSet innerResult2 = innerStatement2.executeQuery();
                innerResult2.next();
                partyFoodDAO.setRestaurantName(innerResult2.getString("Name"));

                foodPartyMenu.add(partyFoodDAO);
                innerResult1.close();
                innerResult2.close();
            }
            result.close();
            statement.close();
            connection.close();
            return foodPartyMenu;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getDeadline(){
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT Deadline FROM FoodParty WHERE FID = 1");
            ResultSet result = statement.executeQuery();
            result.next();
            String deadline = DateTimeConverter.SQLToLocalDateTime(result.getString("Deadline"));
            statement.close();
            connection.close();
            return deadline;
        } catch (SQLException e) {
            e.printStackTrace();
            return "";
        }
    }

    public void incrementCount(String RestaurantId, int FoodId){
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "update PartyFood set `Count` = `Count` + 1 WHERE RestaurantID = ? and FoodID = ? ;");
            statement.setString(1, RestaurantId);
            statement.setInt(2, FoodId);
            statement.executeUpdate();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void decrementCount(String RestaurantId, int FoodId){
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "update PartyFood set `Count` = `Count` - 1 WHERE RestaurantID = ? and FoodID = ? and Count>0;");
            statement.setString(1, RestaurantId);
            statement.setInt(2, FoodId);
            statement.executeUpdate();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean isOver(String RestaurantId, int FoodId){
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT Count From PartyFood WHERE RestaurantID = ? and FoodID = ?;");
            statement.setString(1, RestaurantId);
            statement.setInt(2, FoodId);
            ResultSet result = statement.executeQuery();
            result.next();
            int count = result.getInt("Count");
            result.close();
            statement.close();
            connection.close();
            if(count==0){
                return true;
            }
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public Integer getFoodPartyPrice(String restID, Integer foodID)
    {
        try {
            Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "select  Price from PartyFood where FoodID = ? and RestaurantID = ?;");
            statement.setInt(1, foodID);
            statement.setString(2, restID);
            ResultSet result = statement.executeQuery();
            result.next();
            Integer price = result.getInt("Price");
            result.close();
            statement.close();
            connection.close();
            return price;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


}
