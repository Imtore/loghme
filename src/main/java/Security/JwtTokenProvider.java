package Security;

import Exceptions.NoJwtTokenProvidedException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import java.security.Security;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static Security.SecurityConstants.*;

public class JwtTokenProvider {


    public static String createToken(Integer userId) {

        Claims claims = Jwts.claims().setSubject(Integer.toString(userId));
        Map< String,Object> hm =
                new HashMap< String,Object>();
        hm.put("typ", "JWT");
        hm.put("alg", "HS256");

        Date now = new Date();
        Date validity = new Date(now.getTime() + EXPIRATION_TIME);
        String jwt = Jwts.builder()//
                .setHeader(hm)
                .setClaims(claims)//
                .setIssuedAt(now)//
                .setIssuer("loghme.com")//
                .setExpiration(validity)//
                .signWith(SignatureAlgorithm.HS256, SECRET)//
                .compact();
        return jwt;
    }


    public static String getUserID(String token) {
        return Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody().getSubject();
    }

    public static String resolveToken(HttpServletRequest req) throws NoJwtTokenProvidedException{
        String bearerToken = req.getHeader(HEADER_STRING);
        if (bearerToken != null && bearerToken.startsWith(TOKEN_PREFIX)) {
            return bearerToken.substring(7);
        }
        else
        {
            throw new NoJwtTokenProvidedException("no jwt token provided");
        }
    }

    public static boolean validateToken(String token) throws JwtException,IllegalArgumentException {
            Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token);
            return true;

    }
}
