package Security;

public class SecurityConstants {
    public static final String SECRET = "loghme";
    public static final long EXPIRATION_TIME = 86400000;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String CLIENT_ID = "587251128426-ca6j5e1e0cbuq9cjtm1gqriqvsv0kich.apps.googleusercontent.com";
//    public static final String SIGN_UP_URL = "/users/sign-up";
}
