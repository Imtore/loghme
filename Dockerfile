FROM maven:3.6.3-jdk-11 AS BUILD
COPY src /usr/app/loghme/src
COPY pom.xml /usr/app/loghme
RUN mvn -f /usr/app/loghme/pom.xml clean package

FROM tomcat:9.0.31-jdk11 AS DEPLOY
COPY --from=BUILD /usr/app/loghme/target/Loghme.war /usr/local/tomcat/webapps/
EXPOSE 8080